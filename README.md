# Premiers pas avec _git_

## Installation

### Principe général

    cd
    mkdir TP_git
    cd TP_git
    git clone https://framagit.org/gafhyb/gitCourse.git
    sh gitCourse/install.sh

## Utilisation

    cd ~/TP_git
    source venv/bin/activate
    jupyter notebook gitCourse/Git.ipynb
