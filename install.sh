#!/bin/bash

# Test if virtualenv is installed
if ! [ -x "$(command -v virtualenv)" ]; then
  echo 'Error: virtualenv is not installed.\n\n Check https://virtualenv.pypa.io/en/latest/installation.html' >&2
  exit 1
fi

# Creating virtual env
virtualenv venv -p python3
. venv/bin/activate

# Jupyter install
pip install jupyter

# bash_kernel install
pip install bash_kernel
python -m bash_kernel.install
